export interface Options {
  networkTaskLimit?: NetworkTaskLimit;
}

export interface NetworkTaskLimit {
  'slow-2g'?: number;
  '2g'?: number;
  '3g'?: number;
  '4g'?: number;
}

export interface TaskInfo {
  name: string;
  api: (...args: Array<unknown>) => Promise<any>;
  apiParams: Array<unknown>;
  onSuccess: (resp: any) => void;
  onError: (error: any) => void;
  priority?: number;
}

export interface Task extends TaskInfo {
  id: number;
}

export default class Queue {
  constructor(options?: Options);

  enqueue(taskInfo: TaskInfo): void;

  dequeue(): Task;
}
