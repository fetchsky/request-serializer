export default class Observable {
  constructor(val) {
    this.value = val;
  }

  setValue(val) {
    if (this.value !== val) {
      this.value = val;
      this.raiseChangedEvent(val);
    }
  }

  getValue() {
    return this.value;
  }

  onChange(callBack) {
    this.valueChangedCallback = callBack;
  }

  raiseChangedEvent(val) {
    if (this.valueChangedCallback) {
      this.valueChangedCallback(val);
    }
  }
}
