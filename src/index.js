import Observable from './observer';

export default class Queue {
  constructor(options = {}) {
    this.data = [];
    this.taskLimit = this.getTaskLimit(options.networkTaskLimit);
    this.taskCount = 0;
    this.obs = new Observable();
    this.obs.onChange(() => this.executeTask());
  }

  getTaskLimit(networkTaskLimit = {}) {
    const networkType = navigator?.connection?.effectiveType || 'unknown';
    const taskLimit = networkTaskLimit[networkType];
    switch (networkType) {
      case 'slow-2g':
        return taskLimit || 1;
      case '2g':
        return taskLimit || 2;
      case '3g':
        return taskLimit || 3;
      case '4g':
        return taskLimit || 4;
      default:
        return 3;
    }
  }

  enqueue(taskInfo) {
    this.data.push({
      id: this.generateTaskId(),
      name: taskInfo.name,
      api: taskInfo.api,
      apiParams: taskInfo.apiParams || [],
      successCallback: taskInfo.onSuccess,
      errorCallback: taskInfo.onError,
      priority: taskInfo.priority || 0,
    });
    this.executeTask();
  }

  generateTaskId(length = 10) {
    return Math.floor(
      10 ** (length - 1) +
        Math.random() * 10 ** length -
        10 ** (length - 1) -
        1,
    );
  }

  getPriorityTask() {
    return this.data.reduce((item, p) => {
      if (p.priority && item.priority && p.priority <= item.priority) {
        return p;
      }
      return item.priority ? item : p;
    }, this.data[0]);
  }

  dequeue() {
    const task = this.getPriorityTask();

    if (task && task.id) {
      this.data = this.data.filter((item) => item.id !== task.id);
    }
    return task;
  }

  async executeTask() {
    if (this.taskCount === this.taskLimit || !this.data.length) {
      return;
    }
    const task = this.dequeue();
    if (!task || (task && !task.id)) {
      return;
    }
    this.taskCount += 1;
    try {
      const resp = await task.api(...task.apiParams);
      if (task.successCallback) {
        task.successCallback(resp);
      }
      this.taskCount -= 1;
    } catch (e) {
      if (task.errorCallback) {
        task.errorCallback(e);
      }
      this.taskCount -= 1;
    }
    this.obs.setValue(task.id);
  }
}
